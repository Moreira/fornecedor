
package Model;


public class PessoaJuridica extends Fornecedor {
  
        private String cnpj;
        private String razaoSocial;

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
    
    public PessoaJuridica (String nome, String telefone, String cnpj){
        super (nome, telefone);
        this.cnpj = cnpj;
    }
    
}