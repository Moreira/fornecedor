
package Model;


public class PessoaFisica extends Fornecedor {
    
    private String cpf;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
    
    public PessoaFisica (String nome, String telefone, String cpf){
        super(nome, telefone);
        this.cpf = cpf;
                
        
    }
    
}