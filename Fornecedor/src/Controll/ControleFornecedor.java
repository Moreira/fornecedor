
package Controll;

import Model.*;
import java.util.ArrayList;


public class ControleFornecedor {
    
    private final ArrayList<Fornecedor> listaFornecedores;
    private final ArrayList<Produto> listaProdutos;
    
    public ControleFornecedor (){
        listaFornecedores = new ArrayList<Fornecedor>();
        listaProdutos = new ArrayList<Produto>();
    }
       
    public String adicionar(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa física adicionado com sucesso";
    }
  
    public String adicionar(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso";
    }
    
    public String adicionar (Produto produto){
        listaProdutos.add(produto);
        return "Produto adicionado com sucesso";
    }
    
    public Fornecedor pesquisarNome (String nome){
        for (Fornecedor PessoaFisica : listaFornecedores) {
            if (PessoaFisica.getNome().equalsIgnoreCase(nome)) return PessoaFisica;
        }
        return null;
    }
  
        
            
}


